﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    private bool awaitingStimulus;
    public bool AwaitingStimulus { get { return awaitingStimulus; } }
    public GameFSM currentGameState;

    public StateMachine(bool awaitingStimulus, GameFSM currentGameState)
    {
        this.awaitingStimulus = awaitingStimulus;
        this.currentGameState = currentGameState;
    }

    /// <summary>
    /// Gets stimulation from player.
    /// </summary>
    public void Stimulate()
    {
        awaitingStimulus = false;
        currentGameState = new PlayerMoveState();
        currentGameState.EnterState(this);
    }

    /// <summary>
    /// Moves to next state.
    /// </summary>
    public void NextState()
    {
        currentGameState.NextState();
    }

    /// <summary>
    /// Called when cycle is finished.
    /// </summary>
    public void FinishedCycle()
    {
        awaitingStimulus = true;
    }
}

