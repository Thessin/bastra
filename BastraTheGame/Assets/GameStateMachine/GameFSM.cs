﻿using UnityEngine;
////////////////////////////////  FINITE STATE MACHINE BASE ////////////////////////////////
public abstract class GameFSM
{
    protected StateMachine stateMachine;

    /// <summary>
    /// Things to happen when entering this state.
    /// </summary>
    /// <param name="stateMachine"></param>
    public virtual void EnterState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
        this.stateMachine.currentGameState = this;
    }

    /// <summary>
    /// Things to happen when moving to next state.
    /// </summary>
    public virtual void NextState()
    {
    }
}

////////////////////////////////  FINITE STATE MACHINE CHILDREN ////////////////////////////////
public class PlayerMoveState : GameFSM
{
    public override void EnterState(StateMachine stateMachine)
    {
        base.EnterState(stateMachine);
    }
    public override void NextState()
    {
        base.NextState();
        if (DeckSystem.Instance.CheckIfNextRound())
        {
            this.stateMachine.FinishedCycle();
            return;
        }

        GameFSM state = new EnemyMoveState();
        stateMachine.currentGameState = state;
        state.EnterState(stateMachine);
    }
}

public class EnemyMoveState : GameFSM
{
    public override void EnterState(StateMachine stateMachine)
    {
        base.EnterState(stateMachine);
        if (DeckSystem.Instance.CheckIfNextRound())
        {
            this.stateMachine.FinishedCycle();
            return;
        }

        DeckSystem.Instance.EnemyPlaysCard();
    }
    public override void NextState()
    {
        base.NextState();
        if (DeckSystem.Instance.CheckIfNextRound())
        {
            this.stateMachine.FinishedCycle();
            return;
        }

        stateMachine.FinishedCycle();

        GameFSM state = new PlayerMoveState();
        stateMachine.currentGameState = state;
        state.EnterState(stateMachine);
    }
}