﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum PlayerType
{
    PLAYER,
    ENEMY
}

public class PointsChangedEvent : UnityEvent<PlayerType,int> { }

public class PointsSystem : Singleton<PointsSystem>
{
    private int PlayerPoints;
    private int EnemyPoints;
    
    public PointsChangedEvent OnPointsChange;

    protected override void Awake()
    {
        base.Awake();

        OnPointsChange = new PointsChangedEvent();
    }

    /// <summary>
    /// To add points to the specified player.
    /// </summary>
    /// <param name="To"></param>
    /// <param name="Points"></param>
    public void AddPoints(PlayerType To, int Points)
    {
        switch (To)
        {
            case PlayerType.PLAYER:
                PlayerPoints += Points;
                OnPointsChange?.Invoke(To, PlayerPoints);
                break;
            case PlayerType.ENEMY:
                EnemyPoints += Points;
                OnPointsChange?.Invoke(To, EnemyPoints);
                break;
        }
    }

    /// <summary>
    /// To get points of the specified player.
    /// </summary>
    /// <param name="Who"></param>
    /// <returns></returns>
    public int GetPoints(PlayerType Who)
    {
        int Points = 0;

        switch (Who)
        {
            case PlayerType.PLAYER:
                Points = PlayerPoints;
                break;
            case PlayerType.ENEMY:
                Points = EnemyPoints;
                break;
        }

        return Points;
    }

    /// <summary>
    /// To reset scores of both players.
    /// </summary>
    public void ResetScores()
    {
        PlayerPoints = 0;
        OnPointsChange?.Invoke(PlayerType.PLAYER, PlayerPoints);
        EnemyPoints = 0;
        OnPointsChange?.Invoke(PlayerType.ENEMY, EnemyPoints);
    }
}
