﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : Singleton<GameSystem>
{
    [SerializeField]
    private Transform Canvas;
    
    [SerializeField]
    private GameObject FinishGameUI;

    /// <summary>
    /// Things to do when starting the game.
    /// </summary>
    public void StartGame()
    {
        PointsSystem.Instance.ResetScores();
        DealerSystem.Instance.ResetDealer();
        DeckSystem.Instance.ResetDeck();


        DealerSystem.Instance.StartGame();
    }

    /// <summary>
    /// Things to do when finishing the game.
    /// </summary>
    public void FinishGame()
    {
        DeckSystem.Instance.LastChecks();

        Instantiate(FinishGameUI, Canvas).GetComponent<FinishUI>().SetFinishText("Your score was " + PointsSystem.Instance.GetPoints(PlayerType.PLAYER).ToString() + "! Want to try again?");
    }
}
