﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public enum TurnState
{
    TURN_PLAYER,
    TURN_ENEMY
}

public enum CardPlace
{
    PLACE_ENEMY,
    PLACE_MIDDLE,
    PLACE_PLAYER
}

public class DeckSystem : Singleton<DeckSystem>
{
    private List<CardController> PlayerHand = new List<CardController>();
    private List<CardController> EnemyHand = new List<CardController>();

    private Stack<CardController> MiddleCards = new Stack<CardController>();

    private List<CardController> PlayerEarnedCards = new List<CardController>();
    private List<CardController> EnemyEarnedCards = new List<CardController>();

    [SerializeField]
    private Transform[] PlayerTransform;
    [SerializeField]
    private Transform[] MiddleTransform;
    [SerializeField]
    private Transform[] EnemyTransform;

    [SerializeField]
    private Transform PlayerEarnedTransform;
    [SerializeField]
    private Transform EnemyEarnedTransform;

    public UnityEvent OnPileIsTaken = new UnityEvent();
    public UnityEvent OnBastra = new UnityEvent();
    
    private Queue<ICommand> Moves = new Queue<ICommand>(10);

    private PlayerType LastMoveIsBy;

    private StateMachine FSM;

    protected override void Awake()
    {
        base.Awake();
    }

    /// <summary>
    /// To reset the deck.
    /// </summary>
    public void ResetDeck()
    {
        Moves.Clear();
        PlayerHand.Clear();
        EnemyHand.Clear();
        MiddleCards.Clear();
        PlayerEarnedCards.Clear();
        EnemyEarnedCards.Clear();
        InitializeStateMachine(TurnState.TURN_PLAYER);  // Game starts with player's turn.
    }

    /// <summary>
    /// Initializes the state machine.
    /// </summary>
    /// <param name="Turn"></param>
    private void InitializeStateMachine(TurnState Turn)
    {
        switch (Turn)
        {
            case TurnState.TURN_PLAYER:
                FSM = new StateMachine(true, new PlayerMoveState());
                break;
            case TurnState.TURN_ENEMY:
                FSM = new StateMachine(false, new EnemyMoveState());
                break;
        }
    }

    /// <summary>
    /// Issuing command when a card is played.
    /// </summary>
    /// <param name="PlayedCard"></param>
    /// <param name="Type"></param>
    public void PlayCard(CardController PlayedCard, PlayerType Type)
    {
        if ((!FSM.AwaitingStimulus) && (Type == PlayerType.PLAYER))
            return;

        if (Type == PlayerType.PLAYER)
            FSM.Stimulate();

        if (CheckIfBastra(PlayedCard))
        {
            ICommand Move = new Bastra(PlayedCard, Type);
            Moves.Enqueue(Move);
            Move.Execute();

            OnBastra?.Invoke();
        }
        else if (PlayedCard.GetCardRank() == CardRanks.RANK_J)
        {
            ICommand Move = new TakeThePile(PlayedCard, Type);
            Moves.Enqueue(Move);
            Move.Execute();

            OnPileIsTaken?.Invoke();
        }
        else
        {
            ICommand Move = new MoveToMiddle(PlayedCard, Type, MiddleTransform[0]);
            Moves.Enqueue(Move);
            Move.Execute();
        }

        LastMoveIsBy = Type;
    }

    /// <summary>
    /// AI card play.
    /// </summary>
    public void EnemyPlaysCard()
    {
        if (MiddleCards.Count > 0)
        {
            CardController Card = EnemyHand.Find((x) => x.GetCardRank() == MiddleCards.Peek().GetCardRank());

            if (Card != null)
                PlayCard(Card, PlayerType.ENEMY);
            else
                PlayCard(EnemyHand[Random.Range(0, EnemyHand.Count)], PlayerType.ENEMY);
        }
        else
        {
            PlayCard(EnemyHand[Random.Range(0, EnemyHand.Count)], PlayerType.ENEMY);
        }
    }

    /// <summary>
    /// Removes card from the given PlayerTypes hand.
    /// </summary>
    /// <param name="Card"></param>
    /// <param name="Type"></param>
    public void RemoveCardFromHand(CardController Card, PlayerType Type)
    {
        switch (Type)
        {
            case PlayerType.ENEMY:
                EnemyHand.Remove(EnemyHand.Find((x) => x.GetCardName() == Card.GetCardName()));
                break;
            case PlayerType.PLAYER:
                PlayerHand.Remove(PlayerHand.Find((x) => x.GetCardName() == Card.GetCardName()));
                break;
        }

        Card.SetPlayerTouchableStatus(false);
    }

    /// <summary>
    /// Move the state machine to the next state.
    /// </summary>
    public void MoveToNextState()
    {
        FSM.NextState();
    }

    /// <summary>
    /// Give the cards in the middle to the specified player.
    /// </summary>
    /// <param name="Type"></param>
    public void AddMiddleTo(PlayerType Type)
    {
        switch (Type)
        {
            case PlayerType.ENEMY:
                foreach (var item in MiddleCards)
                {
                    EnemyEarnedCards.Add(item);
                    item.MoveViewTo(EnemyEarnedTransform, false);
                    item.SetPlayerTouchableStatus(false);
                }
                break;
            case PlayerType.PLAYER:
                foreach (var item in MiddleCards)
                {
                    PlayerEarnedCards.Add(item);
                    item.MoveViewTo(PlayerEarnedTransform, false);
                    item.SetPlayerTouchableStatus(false);
                }
                break;
        }

        foreach (var item in MiddleCards)
        {
            PointsSystem.Instance.AddPoints(Type, item.GetCardPoints());
        }

        MiddleCards.Clear();
    }

    /// <summary>
    /// Checks if a Bastra is possible.
    /// </summary>
    /// <param name="CardToCheck"></param>
    /// <returns></returns>
    private bool CheckIfBastra(CardController CardToCheck)
    {
        if (MiddleCards.Count == 0)
            return false;

        return CardToCheck.GetCardRank() == MiddleCards.Peek().GetCardRank();
    }

    /// <summary>
    /// Check if deck is ready to move on to the next round.
    /// </summary>
    /// <returns></returns>
    public bool CheckIfNextRound()
    {
        if ((PlayerHand.Count + EnemyHand.Count) == 0)
        {
            DealerSystem.Instance.DealCards();

            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// To add given cards to a specified place. Used for dealing cards from DealerSystem.
    /// </summary>
    /// <param name="Place"></param>
    /// <param name="Controllers"></param>
    public void AddCardToPlaceList(CardPlace Place, List<CardController> Controllers)
    {
        switch (Place)
        {
            case CardPlace.PLACE_ENEMY:
                for(int i=0;i<Controllers.Count;i++)
                {
                    EnemyHand.Add(Controllers[i]);
                    Controllers[i].MoveViewTo(EnemyTransform[i], false);
                    Controllers[i].SetPlayerTouchableStatus(false);
                }
                break;
            case CardPlace.PLACE_MIDDLE:
                foreach (var item in Controllers)
                {
                    AddCardToMiddle(item, true);
                    item.MoveViewTo(MiddleTransform[0], true);
                    item.SetPlayerTouchableStatus(false);
                }
                break;
            case CardPlace.PLACE_PLAYER:
                for (int i = 0; i < Controllers.Count; i++)
                {
                    PlayerHand.Add(Controllers[i]);
                    Controllers[i].MoveViewTo(PlayerTransform[i], true);
                    Controllers[i].SetPlayerTouchableStatus(true);
                }
                break;
        }
    }

    /// <summary>
    /// What happens when a card is to go to the middle.
    /// </summary>
    /// <param name="Card"></param>
    public void AddCardToMiddle(CardController Card, bool IsNewGame = false)
    {
        if (MiddleCards.Count != 0)
        {
            if (IsNewGame == false)
                MiddleCards.Peek().MoveViewTo(MiddleTransform[1], true);
            else
                MiddleCards.Peek().MoveViewTo(MiddleTransform[2], false);
        }

        MiddleCards.Push(Card);
    }

    /// <summary>
    /// Current deck state in string.
    /// </summary>
    /// <returns></returns>
    public string GetCurrentSituation()
    {
        string Situation = "";

        Situation += "Pile_Cards = ";
        foreach (var cardController in MiddleCards)
        {
            Situation += cardController.GetCardName() + " | ";
        }
        Situation += "\n";

        Situation += "Player_Cards = ";
        foreach (var cardController in PlayerHand)
        {
            Situation += cardController.GetCardName() + " | ";
        }
        Situation += "\n";

        Situation += "Enemy_Cards = ";
        foreach (var cardController in EnemyHand)
        {
            Situation += cardController.GetCardName() + " | ";
        }
        Situation += "\n";

        Situation += "Player_Earned_Cards = ";
        foreach (var cardController in PlayerEarnedCards)
        {
            Situation += cardController.GetCardName() + " | ";
        }
        Situation += "\n";

        Situation += "Enemy_Earned_Cards = ";
        foreach (var cardController in EnemyEarnedCards)
        {
            Situation += cardController.GetCardName() + " | ";
        }
        return Situation;
    }

    /// <summary>
    /// Get all the last moves.
    /// </summary>
    /// <returns></returns>
    public Queue<ICommand> GetLastMoves()
    {
        return Moves;
    }

    /// <summary>
    /// Last checks before ending the game.
    /// </summary>
    public void LastChecks()
    {
        AddMiddleTo(LastMoveIsBy);

        if (PlayerEarnedCards.Count >= EnemyEarnedCards.Count)
            PointsSystem.Instance.AddPoints(PlayerType.PLAYER, 3);
        else
            PointsSystem.Instance.AddPoints(PlayerType.ENEMY, 3);
    }
}
