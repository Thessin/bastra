﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealerSystem : Singleton<DealerSystem>
{
    private List<CardController> CardList = new List<CardController>();
    private List<CardView> CardViewList = new List<CardView>();

    private int DealtIndex = 0;
    private int DealCount = 4;

    [SerializeField]
    private GameObject CardGO;

    [SerializeField]
    private Transform DeckTrans;

    protected override void Awake()
    {
        base.Awake();
    }

    public void ResetDealer()
    {
        CardList.Clear();
        foreach (var item in CardViewList)
        {
            Destroy(item.gameObject);
        }
        CardViewList.Clear();

        DealtIndex = 0;

        CreateCards();
        RandomizeCards();
    }

    private void CreateCards()
    {
        for (int suit = 0; suit < 4; suit++)
        {
            for (int rank = 0; rank < 13; rank++)
            {
                CardList.Add(new CardController((CardSuits)suit, (CardRanks)rank));
            }
        }

        // Delet dis
        foreach (var item in CardList)
        {
            CardView View = Instantiate(CardGO, DeckTrans).GetComponent<CardView>();
            item.AttachToView(View);
            CardViewList.Add(View);
        }
    }

    private void RandomizeCards()
    {
        CardList.Shuffle();
    }

    public void StartGame()
    {
        DealToMiddle();
        DealCards();
    }

    private void DealToMiddle()
    {
        DeckSystem.Instance.AddCardToPlaceList(CardPlace.PLACE_MIDDLE, CardList.GetRange(DealtIndex, DealCount));
        DealtIndex += DealCount;
    }

    public void DealCards()
    {
        if (CheckIfNoCardLeft())
        {
            GameSystem.Instance.FinishGame();
        }
        else
        {
            // Dealing to enemy hand.
            DeckSystem.Instance.AddCardToPlaceList(CardPlace.PLACE_ENEMY, CardList.GetRange(DealtIndex, DealCount));
            DealtIndex += DealCount;

            // Dealing to player hand.
            DeckSystem.Instance.AddCardToPlaceList(CardPlace.PLACE_PLAYER, CardList.GetRange(DealtIndex, DealCount));
            DealtIndex += DealCount;

            Debug.Log("DEALT INDEX = " + DealtIndex.ToString());
        }
    }

    private bool CheckIfNoCardLeft()
    {
        return DealtIndex == CardList.Count;
    }
}
