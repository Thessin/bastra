﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bastra : ICommand
{
    private CardController Card;
    private PlayerType Type;
    private string Situation = "";

    public Bastra(CardController Card, PlayerType Type)
    {
        this.Card = Card;
        this.Type = Type;
    }

    /// <summary>
    /// To execute the command.
    /// </summary>
    public void Execute()
    {
        Debug.Log("Bastra command executed." + " " + Card.GetCardName());

        if (Card.GetCardRank() == CardRanks.RANK_J)
            PointsSystem.Instance.AddPoints(Type, 20);
        else
            PointsSystem.Instance.AddPoints(Type, 10);


        DeckSystem.Instance.AddCardToMiddle(Card);
        DeckSystem.Instance.RemoveCardFromHand(Card, Type);
        DeckSystem.Instance.AddMiddleTo(Type);
        DeckSystem.Instance.MoveToNextState();

        Situation = DeckSystem.Instance.GetCurrentSituation();
        Debug.Log(Situation);
    }

    // To get the last state after this command is executed.
    public string GetLastState()
    {
        return Situation;
    }
}
