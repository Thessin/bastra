﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMiddle : ICommand
{
    private CardController CardToMove;
    private PlayerType Type;
    private Transform Trans;
    private string Situation = "";

    public MoveToMiddle(CardController CardToMove, PlayerType Type, Transform Trans)
    {
        this.CardToMove = CardToMove;
        this.Type = Type;
        this.Trans = Trans;
    }

    public void Execute()
    {
        Debug.Log("MoveToMiddle command executed." + " " + CardToMove.GetCardName());

        CardToMove.MoveViewTo(Trans, true, ()=> DeckSystem.Instance.MoveToNextState());

        DeckSystem.Instance.AddCardToMiddle(CardToMove);
        DeckSystem.Instance.RemoveCardFromHand(CardToMove, Type);

        Situation = DeckSystem.Instance.GetCurrentSituation();
        Debug.Log(Situation);
    }

    public string GetLastState()
    {
        return Situation;
    }
}
