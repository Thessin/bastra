﻿public interface ICommand
{
    void Execute();
    string GetLastState();
}
