﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeThePile : ICommand
{
    private CardController Card;
    private PlayerType Type;
    private string Situation = "";

    public TakeThePile(CardController Card, PlayerType Type)
    {
        this.Type = Type;
        this.Card = Card;
    }

    public void Execute()
    {
        Debug.Log("TakeThePile command executed.");

        DeckSystem.Instance.AddCardToMiddle(Card);
        DeckSystem.Instance.RemoveCardFromHand(Card, Type);
        DeckSystem.Instance.AddMiddleTo(Type);
        DeckSystem.Instance.MoveToNextState();

        Situation = DeckSystem.Instance.GetCurrentSituation();
        Debug.Log(Situation);
    }

    public string GetLastState()
    {
        return Situation;
    }
}
