﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FinishUI : MonoBehaviour
{
    [SerializeField]
    private Button RestartButton;

    [SerializeField]
    private TextMeshProUGUI FinishText;

    private void OnEnable()
    {
        RestartButton.onClick.AddListener(RestartTheGame);
    }

    private void OnDisable()
    {
        RestartButton.onClick.RemoveListener(RestartTheGame);
    }

    /// <summary>
    /// Sets the text on finish screen.
    /// </summary>
    /// <param name="Text"></param>
    public void SetFinishText(string Text)
    {
        FinishText.text = Text;
    }

    /// <summary>
    /// Restarts the game.
    /// </summary>
    private void RestartTheGame()
    {
        GameSystem.Instance.StartGame();
        Destroy(gameObject);
    }
}
