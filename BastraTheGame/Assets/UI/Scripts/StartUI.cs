﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartUI : MonoBehaviour
{
    [SerializeField]
    private Button StartButton;

    private void OnEnable()
    {
        StartButton.onClick.AddListener(StartTheGame);
    }

    private void OnDisable()
    {
        StartButton.onClick.RemoveListener(StartTheGame);
    }

    /// <summary>
    /// To start the game.
    /// </summary>
    private void StartTheGame()
    {
        GameSystem.Instance.StartGame();
        Destroy(gameObject);
    }
}
