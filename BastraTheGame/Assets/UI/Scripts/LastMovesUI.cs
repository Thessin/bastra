﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class LastMovesUI : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private GameObject MoveTextGO;

    [SerializeField]
    private Transform ContentTransform;

    [SerializeField]
    private Image LastMovesPanelImage;

    private bool IsOpen = false;
    private float AnimationDuration = 0.66f;

    /// <summary>
    /// To show the last moves.
    /// </summary>
    private void ShowLastMoves()
    {
        LastMovesPanelImage.DOComplete();
        LastMovesPanelImage.DOFade(1.0f, AnimationDuration);

        foreach(var item in DeckSystem.Instance.GetLastMoves())
        {
            GameObject Obj = Instantiate(MoveTextGO, ContentTransform);
            Obj.GetComponent<TextMeshProUGUI>().text = item.GetLastState();
        }

        IsOpen = true;
    }

    /// <summary>
    /// To close the last moves.
    /// </summary>
    private void CloseLastMoves()
    {
        LastMovesPanelImage.DOComplete();
        LastMovesPanelImage.DOFade(0.03f, AnimationDuration);

        for (int i = 0; i< ContentTransform.childCount; i++)
        {
            Destroy(ContentTransform.GetChild(i).gameObject);
        }

        IsOpen = false;
    }

    /// <summary>
    /// What happens when this UI object is clicked.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (IsOpen)
                CloseLastMoves();
            else
                ShowLastMoves();
        }
    }
}
