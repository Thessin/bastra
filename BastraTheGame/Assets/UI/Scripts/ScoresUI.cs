﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoresUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI EnemyScoreText;
    [SerializeField]
    private TextMeshProUGUI PlayerScoreText;

    private void Start()
    {
        PointsSystem.Instance.OnPointsChange.AddListener(UpdateTexts);
    }

    private void OnDisable()
    {
        PointsSystem.Instance.OnPointsChange.RemoveListener(UpdateTexts);
    }

    /// <summary>
    /// To update the score texts.
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="NewPoint"></param>
    private void UpdateTexts(PlayerType Type, int NewPoint)
    {
        switch (Type)
        {
            case PlayerType.ENEMY:
                EnemyScoreText.text = "Enemy(" + NewPoint.ToString() + ")";
                break;
            case PlayerType.PLAYER:
                PlayerScoreText.text = "Player(" + NewPoint.ToString() + ")";
                break;
        }
    }
}
