﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CardController
{
    private CardModel Model;
    private CardView View;

    private string CardName = "";
    private int? CardPoints = null;

    private bool IsPlayerTouchable = false;

    private float MoveTime = 1.0f;
    private Vector3 OpenRotation = new Vector3(90, 90, 90);
    private Vector3 ClosedRotation = new Vector3(270, 90, 90);

    public CardController(CardSuits CardSuit, CardRanks CardRank)
    {
        Model = new CardModel(CardSuit, CardRank);
    }

    /// <summary>
    /// To attach this controller to a CardView.
    /// </summary>
    /// <param name="View"></param>
    public void AttachToView(CardView View)
    {
        this.View = View;
        this.View.FillCardData(Model, this, GetCardName());
    }

    /// <summary>
    /// To move this view with animation.
    /// </summary>
    /// <param name="Trans"></param>
    /// <param name="IsOpen"></param>
    /// <param name="Callback"></param>
    public void MoveViewTo(Transform Trans, bool IsOpen, TweenCallback Callback = null)
    {
        View.gameObject.transform.DOMove(Trans.position, MoveTime).OnComplete(() => Callback?.Invoke());
        if (IsOpen)
            View.gameObject.transform.DORotate(OpenRotation, MoveTime);
        else
            View.gameObject.transform.DORotate(ClosedRotation, MoveTime);
    }

    /// <summary>
    /// To get this cards name.
    /// </summary>
    /// <returns></returns>
    public string GetCardName()
    {
        if (CardName == "")                 // To define the card's name only for once.
        {
            switch (Model.CardSuit)                   // Adding suit name.
            {
                case CardSuits.SUIT_CLUBS:
                    CardName += "C";
                    break;
                case CardSuits.SUIT_DIAMONDS:
                    CardName += "D";
                    break;
                case CardSuits.SUIT_HEARTS:
                    CardName += "H";
                    break;
                case CardSuits.SUIT_SPADES:
                    CardName += "S";
                    break;
            }

            CardName += "_";                // Adding separator.

            switch (Model.CardRank)                   // Adding rank name.
            {
                case CardRanks.RANK_A:
                    CardName += "A";
                    break;
                case CardRanks.RANK_J:
                    CardName += "J";
                    break;
                case CardRanks.RANK_Q:
                    CardName += "Q";
                    break;
                case CardRanks.RANK_K:
                    CardName += "K";
                    break;
                default:
                    CardName += ((int)Model.CardRank + 1).ToString();
                    break;
            }
        }

        return CardName;
    }

    /// <summary>
    /// To get this cards points.
    /// </summary>
    /// <returns></returns>
    public int GetCardPoints()
    {
        if (CardPoints == null)
        {
            if (Model.CardRank == CardRanks.RANK_A || Model.CardRank == CardRanks.RANK_J) CardPoints = 1;
            else if (Model.CardRank == CardRanks.RANK_2 && Model.CardSuit == CardSuits.SUIT_CLUBS) CardPoints = 2;
            else if (Model.CardRank == CardRanks.RANK_10 && Model.CardSuit == CardSuits.SUIT_DIAMONDS) CardPoints = 3;
        }

        return CardPoints ?? default;   // If CardPoints is null then return default of int which is 0, else return it's value.
    }

    /// <summary>
    /// To get this cards rank.
    /// </summary>
    /// <returns></returns>
    public CardRanks GetCardRank()
    {
        return Model.CardRank;
    }

    /// <summary>
    /// To set the touchable status to prevent or let the player interact with this card.
    /// </summary>
    /// <param name="Status"></param>
    public void SetPlayerTouchableStatus(bool Status)
    {
        IsPlayerTouchable = Status;
    }

    /// <summary>
    /// Get if the player can touch this card.
    /// </summary>
    /// <returns></returns>
    public bool GetPlayerTouchableStatus()
    {
        return IsPlayerTouchable;
    }
}
