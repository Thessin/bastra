﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardSuits
{
    SUIT_CLUBS,
    SUIT_DIAMONDS,
    SUIT_SPADES,
    SUIT_HEARTS
}

public enum CardRanks
{
    RANK_A,
    RANK_2,
    RANK_3,
    RANK_4,
    RANK_5,
    RANK_6,
    RANK_7,
    RANK_8,
    RANK_9,
    RANK_10,
    RANK_J,
    RANK_Q,
    RANK_K
}

public class CardModel
{
    public CardSuits CardSuit { get; }
    public CardRanks CardRank { get; }

    public CardModel(CardSuits CardSuit, CardRanks CardRank)
    {
        this.CardSuit = CardSuit;
        this.CardRank = CardRank;
    }
}
