﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CardView : MonoBehaviour, IPointerClickHandler
{
    private CardModel Model;
    private CardController Controller;

    [SerializeField]
    private SpriteRenderer CardImage;

    private string ResourcesFolderName = "PlayingCards" + Path.DirectorySeparatorChar;

    /// <summary>
    /// Fill this view with the given data.
    /// </summary>
    /// <param name="Model"></param>
    /// <param name="Controller"></param>
    /// <param name="CardName"></param>
    public void FillCardData(CardModel Model, CardController Controller, string CardName)
    {
        this.Model = Model;
        this.Controller = Controller;

        CardImage.sprite = Resources.Load<Sprite>(ResourcesFolderName + CardName);
    }

    /// <summary>
    /// What happens when player clicks on this object.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!Controller.GetPlayerTouchableStatus())
            return;

        DeckSystem.Instance.PlayCard(Controller, PlayerType.PLAYER);
    }
}
